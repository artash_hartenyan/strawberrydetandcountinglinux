/****************************************************************************
** Meta object code from reading C++ file 'VideoShowDemo.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../VideoShowDemo.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'VideoShowDemo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_VideoShowDemo[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x0a,
      34,   14,   14,   14, 0x0a,
      59,   14,   14,   14, 0x0a,
      83,   14,   14,   14, 0x0a,
     116,  107,   14,   14, 0x0a,
     152,  140,   14,   14, 0x0a,
     206,  189,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_VideoShowDemo[] = {
    "VideoShowDemo\0\0testVideoCapture()\0"
    "onPushButtonStart_Stop()\0"
    "onPushbuttonOpenvideo()\0onPushButtonSaveImage()\0"
    "newValue\0onChangeVideoSpeed(int)\0"
    "intersecPer\0onChangeIntersectionAreaPercent(int)\0"
    "minimumStrawSize\0onChangeMinimumStrawSize(int)\0"
};

void VideoShowDemo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VideoShowDemo *_t = static_cast<VideoShowDemo *>(_o);
        switch (_id) {
        case 0: _t->testVideoCapture(); break;
        case 1: _t->onPushButtonStart_Stop(); break;
        case 2: _t->onPushbuttonOpenvideo(); break;
        case 3: _t->onPushButtonSaveImage(); break;
        case 4: _t->onChangeVideoSpeed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->onChangeIntersectionAreaPercent((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->onChangeMinimumStrawSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData VideoShowDemo::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VideoShowDemo::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VideoShowDemo,
      qt_meta_data_VideoShowDemo, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VideoShowDemo::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VideoShowDemo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VideoShowDemo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VideoShowDemo))
        return static_cast<void*>(const_cast< VideoShowDemo*>(this));
    return QWidget::qt_metacast(_clname);
}

int VideoShowDemo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
