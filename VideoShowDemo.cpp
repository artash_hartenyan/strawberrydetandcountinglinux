#include "VideoShowDemo.h"


#include <cmath>
#include <iostream>
#include <sstream>

#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsRectItem>
#include <QPushButton>
#include <QGridLayout>
#include <QSpinBox>
#include <QList>
#include <QTextLayout>
#include <QLabel>
#include <QDebug>
#include <QTimer>
#include <QString>
#include <QFileDialog>
#include <QSlider>
#include <QTableWidget>
#include <QRadioButton>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>



VideoShowDemo::VideoShowDemo(QWidget *parent) :
QWidget(parent)

{
    createMembers();
    setupLayout();
    makeConnections();
    resize(1000, 600);
}

void  VideoShowDemo::createMembers()
{
    //m_optionWidget = new OptionWidget();
//	m_textRegionExtractor = new TextRegionExtractor;
    m_graphicsViewVideo = new QGraphicsView;
    //m_graphicsViewText = new QGraphicsView;
    m_currentRectItem = new QGraphicsRectItem();
    m_scenvideo = new QGraphicsScene;
//	m_scen_text = new QGraphicsScene;
    m_timer = new QTimer;
    m_pushButtonStart_Stop = new QPushButton("START");
    m_pushButtonOpenVideo = new  QPushButton("Open video");
    m_pushButtonOpenVideo->setFixedSize(100, 30);

    m_pushButtonSaveCurrentFrame = new QPushButton("Save Image");
    m_pushButtonSaveCurrentFrame->setFixedSize(100, 30);

    m_videoSpeedSpinBox = new QSpinBox;
    m_videoSpeedSpinBox->setFixedSize(100, 30);
    m_videoSpeedSpinBox->setMinimum(30);
    m_videoSpeedSpinBox->setSingleStep(10);
    m_videoSpeedSpinBox->setMaximum(400);
    m_videoSpeedSpinBox->setValue(100);
    //std::cout << "Before initialize " << std::endl;
    //m_redStrawDetector = RedStrawDetector();

    m_interThreshSpinBox = new QSpinBox;
    m_interThreshSpinBox->setFixedSize(100, 30);
    m_interThreshSpinBox->setMinimum(30);
    m_interThreshSpinBox->setSingleStep(5);
    m_interThreshSpinBox->setMaximum(100);
    m_interThreshSpinBox->setValue(40);


    m_minimumSizeForStrawRectSpinBox = new QSpinBox;
    m_minimumSizeForStrawRectSpinBox->setFixedSize(100, 30);
    m_minimumSizeForStrawRectSpinBox->setMinimum(7);
    m_minimumSizeForStrawRectSpinBox->setSingleStep(5);
    m_minimumSizeForStrawRectSpinBox->setMaximum(100);
    m_minimumSizeForStrawRectSpinBox->setValue(10);



    m_spinTextLabel = new QLabel("Video Speed");
    m_spinTextLabel->setFixedSize(100, 30);

    m_spinInterThreshLabel = new QLabel("Intersection percent");
    m_spinInterThreshLabel->setFixedSize(100, 30);

    m_minimumSizeForStrawRectLabel = new QLabel("Minimum Straw Rect Size");
    m_minimumSizeForStrawRectLabel->setFixedSize(100, 30);

    m_countOfRaspLabel = new QLabel("Count Of Straw 0");
    m_countOfRaspLabel->setFixedSize(150, 30);
    QFont font = m_countOfRaspLabel->font();
    font.setPointSize(10);
    font.setBold(true);
    m_countOfRaspLabel->setFont(font);
    /*Setting of color for label*/
    QPalette* palette1 = new QPalette;
    //palette1->setColor(m_countOfRaspLabel->backgroundRole(), Qt::green);
    palette1->setColor(m_countOfRaspLabel->foregroundRole(), Qt::green);
    m_countOfRaspLabel->setPalette(*palette1);

//	m_optionWidget->setFixedHeight(150);
//	m_graphicsViewText->setStyleSheet("background-color: white;");
    //m_optionWidget->show();
    m_drawRect = false;
    m_currentFrameIndex = 0;
    m_indexOfFrameStartedAlg = 0;
    m_countOfAllRedRasp = 0;
    m_isStartedDetectionAlgorithm = false;
    m_totalCountOfRedStrawberries = 0;
    m_selectedRectsCountOnCurrentFrame = 0;
    m_intersectionAreaPercent = m_interThreshSpinBox->value();
    m_minimumStrawSize = m_minimumSizeForStrawRectSpinBox->value();
}

void  VideoShowDemo::setupLayout()
{
    QGridLayout * glayout = new QGridLayout(this);
    QHBoxLayout * hlayout = new QHBoxLayout();
    QHBoxLayout * spinLayout = new QHBoxLayout();

    hlayout->setGeometry(QRect(20, 20, 300, 30));
    hlayout->setSpacing(10);
    hlayout->setMargin(10);
    hlayout->setContentsMargins(-1, -1, -1, 0);
    hlayout->addWidget(m_pushButtonOpenVideo);
    hlayout->addWidget(m_pushButtonSaveCurrentFrame);
    spinLayout->addWidget(m_spinTextLabel,5);
    spinLayout->addWidget(m_videoSpeedSpinBox);
    spinLayout->addWidget(m_spinInterThreshLabel, 5);
    spinLayout->addWidget(m_interThreshSpinBox);
    spinLayout->addWidget(m_minimumSizeForStrawRectLabel);
    spinLayout->addWidget(m_minimumSizeForStrawRectSpinBox);
    spinLayout->addWidget(m_countOfRaspLabel);

    hlayout->addLayout(spinLayout);
    //vlayout->addLayout(hlayout);
    //vlayout->addWidget(m_pushButtonSaveCurrentFrame);
    glayout->addLayout(hlayout, 0, 2, 1, 1);
//	glayout->addWidget(m_pushButtonOpenVideo, 0, 2, 1, 1);
//	glayout->addWidget(m_spinTextLabel, 0, 3, 2, 2);
//	glayout->addWidget(m_videoSpeedSpinBox, 0, 4, 2, 2);
//	glayout->addWidget(m_pushButtonSaveCurrentFrame,1,2,1,1);
    glayout->addWidget(m_graphicsViewVideo, 2, 2, 1, 1);
    //glayout->addWidget(m_graphicsViewText, 1, 3, 1, 1);
    glayout->addWidget(m_pushButtonStart_Stop, 3, 2, 1, 1);
    //glayout->addWidget(m_optionWidget,3,2,1,3);
    m_graphicsViewVideo->setScene(m_scenvideo);
    m_scenvideo->installEventFilter(this);

//	m_graphicsViewText->setScene(m_scen_text);
//	m_graphicsViewText->resize(m_graphicsViewVideo->size());
    setLayout(glayout);
}

void  VideoShowDemo::makeConnections()
{

    connect(m_timer, SIGNAL(timeout()), this, SLOT(testVideoCapture()));
    connect(m_pushButtonStart_Stop, SIGNAL(clicked()), this, SLOT(onPushButtonStart_Stop()));
    connect(m_pushButtonOpenVideo, SIGNAL(clicked()), this, SLOT(onPushbuttonOpenvideo()));
    connect(m_pushButtonSaveCurrentFrame, SIGNAL(clicked()), this, SLOT(onPushButtonSaveImage()));
    connect(m_videoSpeedSpinBox, SIGNAL(valueChanged(int )), this, SLOT(onChangeVideoSpeed(int)));
    connect(m_interThreshSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onChangeIntersectionAreaPercent(int)));
    connect(m_minimumSizeForStrawRectSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onChangeMinimumStrawSize(int)));
}

void VideoShowDemo::drawCurrentDetectedRectsByAlgorithm()
{
    /*for (QList<QGraphicsRectItem*>::const_iterator it(m_detectedRectsByAlgGraphicsVec.begin()); it != m_detectedRectsByAlgGraphicsVec.end(); ++it) {
    //	std::cout << "current list rect " << (*it)->boundingRect().x() << "  " << (*it)->boundingRect().y() << "  " << (*it)->boundingRect().width() << "  " << (*it)->boundingRect().height();
        //m_graphicsViewVideo->scene()->addRect((*it)->rect(), QPen(QBrush(QColor(0, 255, 0)), 1.0 / 1.0));

        m_graphicsViewVideo->scene()->addRect(QRect(10,10,100,100), QPen(QBrush(QColor(0, 255, 0)),
            1.0 / 1.0));
        std::cout << "ADDED RECT ARTASH   " << (*it)->rect().x() << "  " << (*it)->rect().y() << "  " << (*it)->rect().width() << "  " << (*it)->rect().height() << std::endl;

    }*/
    for (int i = 0; i < m_detectedRectsByAlg.size(); ++i){
    //	std::cout << "Comming rect " << m_detectedRectsByAlg[i].x << "   " << m_detectedRectsByAlg[i].y << "   " << m_detectedRectsByAlg[i].width << "   " << m_detectedRectsByAlg[i].height << std::endl;
        m_graphicsViewVideo->scene()->addRect(QRect(m_detectedRectsByAlg[i].x, m_detectedRectsByAlg[i].y, m_detectedRectsByAlg[i].width, m_detectedRectsByAlg[i].height), QPen(QBrush(QColor(0, 255, 0)),
            1.0 / 1.0));
    }
    //cv::imshow("curr frame", m_currentFrameOrig);
    //cv::waitKey(0);
}

void  VideoShowDemo::checkAndRemoveRectContainedPoint(const QPointF& tmpPoint,unsigned int& indexOfRemovedRect)
{


    int indexOfRem = 0;
    std::vector<cv::Rect2d> currentVecForRemRects;

    std::vector<cv::Rect2d> vectorOfTrackerRects = m_redStrawDetector.m_strawTracker.getRectsFromTrackVector();
    for (int i = 0; i < vectorOfTrackerRects.size(); ++i) {
        if (tmpPoint.x() >= vectorOfTrackerRects[i].x && (tmpPoint.x() <=( vectorOfTrackerRects[i].x + vectorOfTrackerRects[i].width)) &&
            tmpPoint.y() >= vectorOfTrackerRects[i].y && (tmpPoint.y() <= (vectorOfTrackerRects[i].y + vectorOfTrackerRects[i].height))) {
            currentVecForRemRects.push_back(vectorOfTrackerRects[i]);
            //if (m_redStrawDetector.m_strawTracker.getTrackerCurrentRectValidityByIndex(i) == false) {
            //	return;
            //}
            m_redStrawDetector.m_strawTracker.changeCurrentRectValidity(i, false);
            if (m_redStrawDetector.m_strawTracker.getTrackerCurrentRectStateForCounted(i)) {
                //m_redStrawDetector.m_strawTracker.changeCurrentRectStateForCounted(i, false);
                if (m_totalCountOfRedStrawberries > 0) {
                    --m_totalCountOfRedStrawberries;
                }
            }

        }
    }
    if (currentVecForRemRects.size() > 0) {
        m_redStrawDetector.m_strawTracker.removeStrawFromTrack(currentVecForRemRects);
    }
    currentVecForRemRects.clear();

    QList<QGraphicsRectItem*>::iterator it = m_detectedRectsByAlgGraphicsVec.begin();
    while (it != m_detectedRectsByAlgGraphicsVec.end()) {



    //for (QList<QGraphicsRectItem*>::iterator it = m_detectedRectsByAlgGraphicsVec.begin(); it != m_detectedRectsByAlgGraphicsVec.end(); ++it) {
        if (tmpPoint.x() >= (*it)->rect().x() && tmpPoint.x() <= ((*it)->rect().x() + (*it)->rect().width()) &&
            tmpPoint.y() >= (*it)->rect().y() && tmpPoint.y() <= ((*it)->rect().y() + (*it)->rect().height())) {
            std::cout << "Want to remove tracked rect !!!!!!!!" << std::endl;
            if (it == m_detectedRectsByAlgGraphicsVec.end()) {
                indexOfRem = it - m_detectedRectsByAlgGraphicsVec.begin();
                //m_detectedRectsByAlgGraphicsVec.at(indexOfRem)->setBrush(QBrush(QColor(0, 0, 255)));
                m_graphicsViewVideo->scene()->removeItem((*it));
                //std::cout << "After remove from scene " << std::endl;
                m_detectedRectsByAlgGraphicsVec.pop_back();
                ++it;
                continue;
                //it = m_detectedRectsByAlgGraphicsVec.erase(it);
            //	break;


            }

        else if (it == m_detectedRectsByAlgGraphicsVec.begin()) {
            m_graphicsViewVideo->scene()->removeItem((*it));
        //	std::cout << "After remove from scene " << std::endl;
            m_detectedRectsByAlgGraphicsVec.pop_front();
        //	if (m_detectedRectsByAlgGraphicsVec.at(indexOfRem)->brush().color == QColor(0,0,255))
            indexOfRem = it - m_detectedRectsByAlgGraphicsVec.begin();
        //	m_detectedRectsByAlgGraphicsVec.at(indexOfRem)->setBrush(QBrush(QColor(0, 0, 255)));
        //	it = m_detectedRectsByAlgGraphicsVec.erase(it);
        //	break;
            ++it;
            continue;

        }

        else {
            m_graphicsViewVideo->scene()->removeItem((*it));
            indexOfRem = it - m_detectedRectsByAlgGraphicsVec.begin();
        //	m_detectedRectsByAlgGraphicsVec.at(indexOfRem)->setBrush(QBrush(QColor(0, 0, 255)));
            //m_rectsItem.pop_back();
        //	std::cout << "After remove from scene " << std::endl;
            it = m_detectedRectsByAlgGraphicsVec.erase(it);
            continue;
        //	break;


        }
    }
        ++it;
    //++indexOfRem;
    }

    m_countOfAllRedRasp = m_detectedRectsByAlgGraphicsVec.size() + m_rectsItem.size();
    std::ostringstream textForCountOfRasbCh;
    textForCountOfRasbCh << m_totalCountOfRedStrawberries;
    std::string textForCountOfRasb = "Count Of Straw " +  textForCountOfRasbCh.str()/*std::to_string(long(m_totalCountOfRedStrawberries))*/;

    if (m_isStartedDetectionAlgorithm) {
        m_countOfRaspLabel->setText(QString(textForCountOfRasb.c_str()));
    }
    indexOfRemovedRect = indexOfRem;
    std::cout << "   ARTASH    double " << std::endl;
    std::cout << "Index of removed item " << indexOfRemovedRect << std::endl;

    std::vector<cv::Rect>::iterator  detRectIt = m_detectedRectsByAlg.begin();

    int startIndex = 0;
    if (indexOfRemovedRect < m_detectedRectsByAlg.size()) {
        while (startIndex != indexOfRemovedRect) {
            ++startIndex;
            ++detRectIt;
        }

        detRectIt = m_detectedRectsByAlg.erase(detRectIt);
    }
    std::cout << "  size of vector " << m_detectedRectsByAlg.size() << std::endl;


}
void VideoShowDemo::testVideoCapture()
{
    //m_timer->stop();
    //m_rectsItem.clear();
    m_detectedRectsByAlgGraphicsVec.clear();
    m_detectedRectsByAlg.clear();
    m_selectedRectsCountOnCurrentFrame = 0;
//	m_selectedRectByUser.clear();
    if (!m_videoCapture.isOpened()) {
        std::cout << "Video capture is not valid \n";
        exit(1);
    }
    cv::Mat frame;// = cv::imread("D:\\Scopic Projects\\x64\\Release\\imageData\\278_orig.png");
    m_videoCapture >> frame>>frame;
    std::cout << "Frame size " << frame.cols << "   " << frame.rows << std::endl;
    if (frame.empty()){
        //m_timer->stop();

        return;
    }
    //cv::resize(frame, frame, cv::Size(),	1.5, 1.5);
    //frame = ~frame;
    static int i = 0;
    m_currentFrameOrig = frame.clone();


//	m_textRegionExtractor->setFrame(frame);

    m_currentFrame = frame.clone();

    m_redStrawDetector.setCurrentImg(frame);
    m_redStrawDetector.detectStrawOnImage();
    m_redStrawDetector.setDetectedRecsVec(m_detectedRectsByAlg);
    //std::cout << "Before filtering detected rectangles size " << m_detectedRectsByAlg.size() << std::endl;
    filterDetectedRectsByAlgorithmInRange(m_detectedRectsByAlg);
    //std::cout << "After filtering detected rectangles size " << m_detectedRectsByAlg.size() << std::endl;


    cv::cvtColor(frame, frame, CV_BGR2RGB);
    QPixmap p = QPixmap::fromImage(QImage((uchar*)frame.data, frame.cols, frame.rows, frame.step1(), QImage::Format_RGB888));

    m_scenvideo->clear();
    m_scenvideo->setSceneRect(p.rect());
    m_scenvideo->addPixmap(p);


    if (m_isStartedDetectionAlgorithm) {
        ++m_indexOfFrameStartedAlg;
        for (int i = 0; i < m_detectedRectsByAlg.size(); ++i) {
            QGraphicsRectItem* currentRectFromAlg = new QGraphicsRectItem;
            currentRectFromAlg->setRect((qreal)m_detectedRectsByAlg[i].x, (qreal)m_detectedRectsByAlg[i].y, (qreal)m_detectedRectsByAlg[i].width, (qreal)m_detectedRectsByAlg[i].height);
            currentRectFromAlg->setPen(QPen(QColor(0, 255, 0)));
        //	m_graphicsViewVideo->scene()->addRect(QRect(m_detectedRectsByAlg[i].x, m_detectedRectsByAlg[i].y, m_detectedRectsByAlg[i].width, m_detectedRectsByAlg[i].height), QPen(QBrush(QColor(0, 255, 0)), 1.0 / 1.0));
            m_detectedRectsByAlgGraphicsVec.push_back(currentRectFromAlg);
            m_graphicsViewVideo->scene()->addItem(currentRectFromAlg);

        }
        m_countOfAllRedRasp = m_detectedRectsByAlgGraphicsVec.size() + m_rectsItem.size();

        m_redStrawDetector.m_strawTracker.setCurrentImage(m_currentFrame);
        /*Added user selected vects to tracker*/
        if (m_selectedRectByUser.size() != 0) {
            std::cout << "User selected rects size!!!!!!!!!!!!!!!! " << m_selectedRectByUser.size() << std::endl;
            std::vector<cv::Rect2d> rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
            for (int k = 0; k < m_selectedRectByUser.size(); ++k) {
                if (!isCurrentRectExistsInTrackedRects(cv::Rect2d(m_selectedRectByUser[k]), rectsVecFromTracker, m_intersectionAreaPercent) && !m_isCurrentUserSelRectAddedToTracker[k]) {
                    m_redStrawDetector.m_strawTracker.addNewStrawToTrack(cv::Rect2d(m_selectedRectByUser[k].x, m_selectedRectByUser[k].y, m_selectedRectByUser[k].width, m_selectedRectByUser[k].height));
                    m_isCurrentUserSelRectAddedToTracker[k] = true;
                }
            }

        }
        /*End of user selected rects to tracker*/
        if (m_indexOfFrameStartedAlg == 1) {
            /*Add rectangles into tracker from first frame*/
            for (int k = 0; k < m_detectedRectsByAlg.size(); ++k) {
                m_redStrawDetector.m_strawTracker.addNewStrawToTrack(cv::Rect2d(m_detectedRectsByAlg[k].x, m_detectedRectsByAlg[k].y, m_detectedRectsByAlg[k].width, m_detectedRectsByAlg[k].height));
            }

            /*Adding user selected rects */


            std::vector<cv::Rect2d> rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
            /*End of adding into tracker*/

        //	m_timer->stop();
        }

        else {
            m_redStrawDetector.m_strawTracker.updateTrackerInfo(m_currentFrame);
            std::vector<cv::Rect2d> rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
            std::vector<cv::Rect2d> rectsVec = m_redStrawDetector.m_strawTracker.getRectsFromTrackVector();
            if (rectsVec.size() == rectsVecFromTracker.size()) {
                std::cout << "size of vects are equal " << std::endl;
            }
            std::cout << "User selected rects size!!!!!!!!!!!!!!!! " << m_selectedRectByUser.size() << std::endl;;
            std::vector<cv::Rect2d> rectsForRemove;
            for (int i = 0; i < rectsVecFromTracker.size(); ++i) {
                if (!isRectInRange(rectsVecFromTracker[i])) {
                    rectsForRemove.push_back(m_redStrawDetector.m_strawTracker.getTrackerCurrentRectByIndex(i));
                }
            }
            if (rectsForRemove.size() != 0) {
        //		std::cout << "Artash  !!!!!!!!!!!!!!!!! go to !!!!!!!!!!!!!!!" << std::endl;
                std::cout << "before remove rects size " << m_redStrawDetector.m_strawTracker.getTrackedStrawberries().size() << std::endl;
                m_redStrawDetector.m_strawTracker.removeStrawFromTrack(rectsForRemove);
                rectsVecFromTracker.clear();
                rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
                std::cout << "after remove rects size " << m_redStrawDetector.m_strawTracker.getTrackedStrawberries().size() << std::endl;
            }
            //m_redStrawDetector.m_strawTracker.removeStrawFromTrack(m_detectedRectsByAlg);
            for (int g = 0; g < m_detectedRectsByAlg.size(); ++g) {
                if (!isCurrentRectExistsInTrackedRects(cv::Rect2d(m_detectedRectsByAlg[g]), rectsVecFromTracker,m_intersectionAreaPercent)) {
                    std::cout << "Info rect and trakcer rects " << std::endl;
                    std::cout << "Tracker rects  " << std::endl;
                    for (int h = 0; h < rectsVecFromTracker.size(); ++h) {
                        std::cout << rectsVecFromTracker[h].x << "   " << rectsVecFromTracker[h].y << "  " << rectsVecFromTracker[h].width << "   " << rectsVecFromTracker[h].height << std::endl;
                    }
                    //std::cout << "Detected rect into " << std::endl;
                    std::cout << m_detectedRectsByAlg[g].x << "   " << m_detectedRectsByAlg[g].y << "   " << m_detectedRectsByAlg[g].width << "   " << m_detectedRectsByAlg[g].height << std::endl;
                    if (m_detectedRectsByAlg[g].width > m_minimumStrawSize && m_detectedRectsByAlg[g].height > m_minimumStrawSize) {
                        std::cout << "CURRENT RECT IS GREATER THAN SIZE !!!!!!" << std::endl;
                        m_redStrawDetector.m_strawTracker.addNewStrawToTrack(cv::Rect2d(m_detectedRectsByAlg[g].x, m_detectedRectsByAlg[g].y, m_detectedRectsByAlg[g].width, m_detectedRectsByAlg[g].height));
                    }
                //	std::cout << "Adding new rect to existing tracker rect!!!!!!!!!!!!!!!! " << std::endl;
                }
            }
            for (int i = 0; i < rectsVecFromTracker.size(); ++i) {
                QGraphicsRectItem* currentRectFromAlg = new QGraphicsRectItem;
            //	std::cout << "After update fnction " << std::endl;
                currentRectFromAlg->setRect((qreal)rectsVecFromTracker[i].x, (qreal)rectsVecFromTracker[i].y, (qreal)rectsVecFromTracker[i].width, (qreal)rectsVecFromTracker[i].height);
                if (m_redStrawDetector.m_strawTracker.getTrackerCurrentRectValidityByIndex(i)) {
                    currentRectFromAlg->setPen(QPen(QColor(255, 0, 0)));
                }
                else {
                    currentRectFromAlg->setBrush((QColor(0, 0, 255)));
                }
                m_detectedRectsByAlgGraphicsVec.push_back(currentRectFromAlg);
                m_graphicsViewVideo->scene()->addItem(currentRectFromAlg);


            }
        //	m_timer->stop();
        }

        /*Total calculation part */
        for (int g = 0; g < m_redStrawDetector.m_strawTracker.getRectsFromTrackVector().size(); ++g) {
            if (m_redStrawDetector.m_strawTracker.getTrackerCurrentRectStateForCounted(g) == false && m_redStrawDetector.m_strawTracker.getTrackerCurrentRectValidityByIndex(g) == true) {
                m_redStrawDetector.m_strawTracker.changeCurrentRectStateForCounted(g, true);
                ++m_totalCountOfRedStrawberries;
            }
        }

        std::ostringstream textForCountOfRasbCh;
        textForCountOfRasbCh << m_totalCountOfRedStrawberries;
        std::string textForCountOfRasb = "Count Of Straw " +  textForCountOfRasbCh.str()/*std::to_string(long(m_totalCountOfRedStrawberries))*/;
        m_countOfRaspLabel->setText(QString(textForCountOfRasb.c_str()));
    }

    ++m_currentFrameIndex;



}
/*
void VideoShowDemo::extractRectClusters(const std::vector<cv::Rect>& inputRects, std::vector<lineOfRects>& clusterOfLines)
{
    clusterOfLines.clear();
    cv::Rect tmpRect;
    for (int i = 0; i < inputRects.size(); ++i) {
        tmpRect = inputRects.at(i);
        if (clusterOfLines.size() == 0) {
            lineOfRects tmpLine;
            tmpLine.push_back(tmpRect);
            clusterOfLines.push_back(tmpLine);
        }
        else {
            for (int j = 0; j < clusterOfLines.size(); ++j) {
                for (int h = 0; h < clusterOfLines.at(j).size(); ++h) {

                    if (((tmpRect.y > clusterOfLines.at(j).at(h).y && (tmpRect.y - clusterOfLines.at(j).at(h).y) > clusterOfLines.at(j).at(h).height)) ||
                        ((tmpRect.y < clusterOfLines.at(j).at(h).y && (clusterOfLines.at(j).at(h).y - tmpRect.y) > tmpRect.height))  ) {
                        if (h == clusterOfLines.at(j).size() - 1){
                            lineOfRects tmpLine;
                            tmpLine.push_back(clusterOfLines.at(j).at(h));
                            clusterOfLines.push_back(tmpLine);
                            break;
                        }
                        else {
                            continue;
                        }

                    }
                    else {
                        clusterOfLines.at(j).push_back(tmpRect);
                        break;
                    }

                }
            }
        }
    }

}*/

void VideoShowDemo::onPushButtonStart_Stop()
{
    m_isStartedDetectionAlgorithm = true;

    if (m_pushButtonStart_Stop->text() == "START"){
        m_timer->start(m_videoSpeedSpinBox->value());

        m_pushButtonStart_Stop->setStyleSheet("QPushButton {background-color: #48D1CC; color: red;}");
        m_pushButtonStart_Stop->setText("STOP");
        return;
    }
    if (m_pushButtonStart_Stop->text() == "STOP"){
        m_timer->stop();
        m_pushButtonStart_Stop->setStyleSheet("QPushButton {background-color: #48D1CC; color: blue;}");
        m_pushButtonStart_Stop->setText("START");
    }
}

/*void VideoShowDemo::graphicsText(const TextRegionExtractor::KeywordIdSentenceRectTupleCollection& rt)
{
    QRect posrect;
    QPen pen(Qt::green, 2, Qt::SolidLine, Qt::MPenCapStyle, Qt::MPenJoinStyle);
    //m_scen_text->clear();

    for (int i = 0; i < rt.size(); ++i){
        posrect = QRect(rt.at(i).m_rect.x, rt.at(i).m_rect.y, rt.at(i).m_rect.width, rt.at(i).m_rect.height);
        QString text = QString::fromStdString(rt.at(i).m_sentence);
        QPixmap pixmap = getStringImg(text, posrect);
        QGraphicsPixmapItem* item1 = new QGraphicsPixmapItem(pixmap);
        item1->setPos(posrect.x(), posrect.y());
    //	m_scen_text->addItem(item1);
        m_scenvideo->addRect(posrect, pen);

    }

}*/

void VideoShowDemo::onPushbuttonOpenvideo()
{
    filename = QFileDialog::getOpenFileName(
        this,
        tr("Open video"),
        ".",
        "Video files (*.avi *.mp4 *.png *.avi)");

    if (filename.isEmpty()) {
        m_timer->stop();
        return;
    }

    m_videoCapture.open(filename.toStdString());
    m_currentFrameIndex = 0;
    //Clearing all previous data
    m_countOfAllRedRasp = 0;
    m_currentFrameIndex = 0;
    m_detectedRectsByAlg.clear();
    m_detectedRectsByAlgGraphicsVec.clear();
    m_drawRect = false;
    m_indexOfFrameStartedAlg = 0;
    m_isStartedDetectionAlgorithm = false;
    m_totalCountOfRedStrawberries = 0;
    m_selectedRectsCountOnCurrentFrame = 0;
    m_selectedRectByUser.clear();
    std::vector<cv::Rect2d> vectorOfTrackerRects = m_redStrawDetector.m_strawTracker.getRectsFromTrackVector();
    m_redStrawDetector.m_strawTracker.removeStrawFromTrack(vectorOfTrackerRects);

    std::ostringstream textForCountOfRasbCh;
    textForCountOfRasbCh << m_totalCountOfRedStrawberries;
    std::string textForCountOfRasb = "Count Of Straw " + textForCountOfRasbCh.str()/*std::to_string(long(m_totalCountOfRedStrawberries))*/;
    m_countOfRaspLabel->setText(QString(textForCountOfRasb.c_str()));
    // End of clearing
    if (!m_videoCapture.isOpened()) {
        qDebug() << "VideoCapture is not openned fileame is not valid" << filename;
        m_timer->stop();
        return;
    }
    m_timer->start(m_videoSpeedSpinBox->value());
}

void VideoShowDemo::onPushButtonSaveImage()
{
    std::cout << "saving image " << std::endl;

    std::ostringstream currentFrameIndexCh;
    currentFrameIndexCh << m_currentFrameIndex;
    std::string savedImagePath = "imageData\\" + currentFrameIndexCh.str()/*std::to_string(long(m_currentFrameIndex))*/ + ".png";
    std::string savedOrigImagePath = "imageData\\" + currentFrameIndexCh.str()/*std::to_string(long(m_currentFrameIndex))*/ + "_orig.png";
    QList<QGraphicsRectItem*>::iterator it;

    cv::Mat saveImg = m_currentFrame.clone();
    if (saveImg.cols != 0 && saveImg.rows != 0) {

        cv::imwrite(savedOrigImagePath, m_currentFrameOrig);
    }
    for ( it = m_rectsItem.begin(); it != m_rectsItem.end(); ++it){
        /*float scaleX = ((float)(m_scenvideo->width())) / ((float)(*it)->rect().x());
        float scaleY = (float)m_scenvideo->height() /(float) (*it)->rect().y();
        float scaleWidth = (float)m_scenvideo->width() / (float) (*it)->rect().width();
        float scaleHeight =(float) m_scenvideo->height() / (float)(*it)->rect().height();
        std::cout << "Scale parameters " << scaleX << "  " << scaleY << "  " << scaleWidth << "   " << scaleHeight << std::endl;
        std::cout << "Scene parameters " << m_scenvideo->width() << "   " << m_scenvideo->height() << std::endl;
        std::cout << "Rect parameters " << (*it)->rect().x()<< "  " << (*it)->rect().y()<< "  " << (*it)->rect().width() << "  " << (*it)->rect().height() << std::endl;
        */
        //std::cout << "Current list item init" << std::endl;
        cv::Rect drawRect;
        /*drawRect.x = (float)m_currentFrame.cols / scaleX;
        drawRect.x = (float)m_currentFrame.rows / scaleY;
        drawRect.width =(float) m_currentFrame.cols / scaleWidth;
        drawRect.height = (float) m_currentFrame.rows / scaleHeight;*/
        drawRect.x = (*it)->rect().x();
        drawRect.y = (*it)->rect().y();
        drawRect.width = (*it)->rect().width();
        drawRect.height = (*it)->rect().height();
        //std::cout << "Current list item end" << std::endl;
        cv::rectangle(saveImg, drawRect, cv::Scalar(0, 0, 255), 2);
    }



    std::vector<cv::Rect2d> rectsVecFromTracker = m_redStrawDetector.m_strawTracker.getTrackedStrawberries();
    for (int i = 0; i < rectsVecFromTracker.size(); ++i) {
        cv::Rect tmpRect;
        tmpRect.x = rectsVecFromTracker[i].x;
        tmpRect.y = rectsVecFromTracker[i].y;
        tmpRect.width = rectsVecFromTracker[i].width;
        tmpRect.height = rectsVecFromTracker[i].height;
        cv::rectangle(saveImg, tmpRect, cv::Scalar(0, 0, 255), 2);
    }

    for (it = m_detectedRectsByAlgGraphicsVec.begin(); it != m_detectedRectsByAlgGraphicsVec.end(); ++it) {
        cv::Rect detectedRect;
        detectedRect.x = (*it)->rect().x();
        detectedRect.y = (*it)->rect().y();
        detectedRect.width = (*it)->rect().width();
        detectedRect.height = (*it)->rect().height();
        cv::rectangle(saveImg, detectedRect, cv::Scalar(0, 255, 0), 2);
    }
    std::cout << "saving image  with path " << savedImagePath << std::endl;
    if (saveImg.cols != 0 && saveImg.rows != 0) {

        cv::imwrite(savedImagePath, saveImg);
    }
}

void VideoShowDemo::onChangeVideoSpeed(int newValue)
{
    int getSpinBoxCurrentValue = m_videoSpeedSpinBox->value();
    m_timer->setInterval(getSpinBoxCurrentValue);
}

void VideoShowDemo::onChangeIntersectionAreaPercent(int intersecPer)
{
    m_intersectionAreaPercent = intersecPer;

}

void VideoShowDemo::onChangeMinimumStrawSize(int minimumStrawSize)
{
    m_minimumStrawSize = minimumStrawSize;
}

QPixmap VideoShowDemo::getStringImg(const QString& str, const QRect& rect)
{
    QString qStr = str;
    QStringList qStrList = qStr.split("\n");
    int maxLength = qStrList.at(0).length();
    for (int i = 0; i < qStrList.size(); ++i) {
        if (qStrList.at(i).length() > maxLength) {
            maxLength = qStrList.at(i).length();
        }
    }

    QFont font("times", rect.width() / maxLength);
    QPixmap pixmap(rect.width() +4, rect.height()+4 );
    pixmap.fill(QColor(255, 255, 255, 255));
    QPainter painter(&pixmap);
    QPen pen(QColor(255, 0, 0, 255));
    painter.setPen(pen);
    painter.setFont(font);
    //painter.drawText(pixmap.rect(), Qt::AlignHCenter, qStr, &pixmap.rect());

    return pixmap;
}

bool VideoShowDemo::isRectInRange(const cv::Rect& currentRect)
{
    if (currentRect.width == 0 || currentRect.height == 0) {
        return false;
    }
    if (currentRect.x > 20 && currentRect.y > 20 && ((currentRect.x + currentRect.width) < (m_currentFrame.cols - 20)) && ((currentRect.y + currentRect.height) < (m_currentFrame.rows - 20))) {
        return true;
    }
    else {
        return false;
    }

}

bool VideoShowDemo::isRectsAreIntersecting(const cv::Rect2d& rect1, const cv::Rect2d& rect2)
{
    if ((rect1.x + rect1.width < rect2.x) || (rect2.x + rect2.width < rect1.x)
        || (rect1.y + rect1.height < rect2.y) || (rect2.y + rect2.height < rect1.y)) {
        return false;
    }
    return true;
/*	if (rect1.x < rect2.x && rect1.x + rect1.width < rect2.x) {
        std::cout << "reason 1 not  intersecting " << std::endl;
        return false;
    } else if (rect1.x > rect2.x && rect2.x + rect2.width < rect1.x) {
        std::cout << "reason 2 not  intersecting " << std::endl;
        return false;
     }
    if (rect1.y < rect2.y && rect1.y + rect1.height < rect2.y) {
        std::cout << "reason 3 not  intersecting " << std::endl;
        return false;
    }
    else if (rect2.y < rect1.y && rect2.y + rect2.height < rect1.y) {
        std::cout << "reason 4 not  intersecting " << std::endl;
        return false;
     }
    return true;*/
}


bool VideoShowDemo::isRectsContainingEachOther(const cv::Rect2d& rect1, const cv::Rect2d& rect2)
{
    if (!isRectsAreIntersecting(rect1, rect2)) {
        return false;
    }
    else if((rect1.x > rect2.x && rect1.y > rect2.y && (rect1.width + rect1.x < rect2.width  + rect2.x) && (rect1.height + rect1.y < rect2.height + rect2.y))
        ||
        (rect1.x < rect2.x && rect1.y < rect2.y && (rect1.width  + rect1.x> rect2.width + rect2.x) && (rect1.height + rect1.y > rect2.height + rect2.y))) {

        return true;

    }
    return false;
}

bool VideoShowDemo::isRectsMuchCloseToEachOther(const cv::Rect2d& rect1, const cv::Rect2d& rect2, const unsigned int& thresholdValue)
{
    double area12 = (float)rect1.area() / (float)rect2.area() * 100;
    double area21 = (float)rect2.area() / (float)rect1.area() * 100;
    if (isRectsContainingEachOther(rect1, rect2)) {
        return true;
    }
    if (isRectsAreIntersecting(rect1, rect2)) {
        //std::cout << "RECTS ARE INTRSECTING " << std::endl;
        if (rect1.area() <= rect2.area() &&
            (((float)rect1.area() / (float)rect2.area() * 100) > thresholdValue) /*|| (((float)rect1.area() / (float)rect2.area() * 100) > thresholdValue)*/) {
            //  std::cout << "AREAAAAAAAAA12 " << area12 << std::endl;
            return true;
        }
        else if (rect1.area() > rect2.area() && (((float)rect2.area() / (float)rect1.area() * 100) > thresholdValue)) {
            //std::cout << "AREAAAAAAAAA22 " << area21 << std::endl;
            return true;
        }
        else {
            return false;
        }
    }
    else {
    //	std::cout << "Reason for adding rects are not intersection " << std::endl;
        return false;
    }
}

bool   VideoShowDemo::isCurrentRectExistsInTrackedRects(const cv::Rect2d& currentRect, const std::vector<cv::Rect2d>& trackerRectsVec, const unsigned int& thresholdValue)
{
    int countOfCompared = 0;
    for (int i = 0; i < trackerRectsVec.size(); ++i) {
        if (isRectsMuchCloseToEachOther(currentRect, trackerRectsVec[i], thresholdValue)) {

            return true;
        }

    }
    return false;
}


void VideoShowDemo::filterDetectedRectsByAlgorithmInRange(std::vector<cv::Rect>& inputRectsVec)
{
    std::vector<cv::Rect>::iterator iterVec;
    for (iterVec = inputRectsVec.begin(); iterVec != inputRectsVec.end();) {
        if (isRectInRange(*iterVec)) {
            ++iterVec;
        }
        else {
            iterVec = m_detectedRectsByAlg.erase(iterVec);
        }
    }
}

bool VideoShowDemo::eventFilter(QObject* object, QEvent* event)
{

    if (m_pushButtonStart_Stop->text() == "STOP" || !m_isStartedDetectionAlgorithm) {

        return false;
    }
    //if (m_imgFileInfoList.isEmpty()) return false;
    //std::cout << "Graphics view size "<<m_scenvideo->
    static QPointF rectStartPoint;

    QGraphicsSceneMouseEvent* ev = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
    if (object == m_scenvideo && ev != 0) {

        //if (ev->type() == QEvent::GraphicsSceneMouseDoubleClick && ev->button() == Qt::LeftButton){
        //	slotNextImage();
        //}

        QGraphicsItem* item = m_scenvideo->itemAt(ev->scenePos(), QTransform());
        qDebug() << ev << item;
        QGraphicsPixmapItem* imgItem = qgraphicsitem_cast<QGraphicsPixmapItem*>(item);
        if (imgItem == 0){
            QGraphicsRectItem* rectItem = qgraphicsitem_cast<QGraphicsRectItem*>(item);
            if (rectItem == 0){
                if (ev->type() == QEvent::GraphicsSceneMouseRelease && ev->button() == Qt::LeftButton ) {
                    if (m_currentRectItem != 0) {
                        if ((ev->scenePos().x() < m_graphicsViewVideo->scene()->sceneRect().width() - 20)
                            && (ev->scenePos().y() < m_graphicsViewVideo->scene()->sceneRect().height() - 20)) {
                            //std::cout << "CURRENT SLECTED RECT LAST POINT " << ev->pos().x() << std::endl;


                            QGraphicsRectItem* rectItem = m_graphicsViewVideo->scene()->addRect(
                                m_currentRectItem->rect(), QPen(QBrush(QColor(128, 0, 255)),
                                    1.0 / 1.0));
                            m_rectsItem.push_back(rectItem);
                            m_isCurrentUserSelRectAddedToTracker.push_back(false);

                        }


                        m_scenvideo->removeItem(m_currentRectItem);
                        m_currentRectItem = 0;
                    }

                }
                m_drawRect = false;
                return QWidget::eventFilter(object, event);
            }
        }

        if (ev->type() == QEvent::GraphicsSceneMousePress && ev->button() == Qt::LeftButton  ) {
            rectStartPoint = ev->scenePos();
            if (rectStartPoint.x() > 20 && rectStartPoint.y() > 20) {
                m_drawRect = true;
            }
        }
        else if (ev->type() == QEvent::GraphicsSceneMouseMove && m_drawRect   ) {

            QPointF tl, br;
            qreal diffx = ev->scenePos().x() - rectStartPoint.x();
            qreal diffy = diffx; //ev->scenePos().y() - rectStartPoint.y();
            /*if (ev->scenePos().y() < rectStartPoint.y()){
            if (diffx > 0.0) diffy = -diffx;
            }
            else {
            if (diffx < 0.0) diffy = -diffx;
            }*/
            tl.setX(std::min(rectStartPoint.x(), rectStartPoint.x() + diffx));
            tl.setY(std::min(rectStartPoint.y(), rectStartPoint.y() + diffy));

            br.setX(std::max(rectStartPoint.x(), rectStartPoint.x() + diffx));
            br.setY(std::max(rectStartPoint.y(), rectStartPoint.y() + diffx));
            //std::cout << "CURRENT SLECTED RECT LAST POINT " <<rectStartPoint.x() << std::endl;
            //	br.setY(std::max(rectStartPoint.y(), rectStartPoint.y() + diffy));
            if (m_scenvideo->itemAt(tl, QTransform()) != 0
                && m_scenvideo->itemAt(br, QTransform()) != 0){
                QRectF r(tl, br);
                qDebug() << r;
                if (m_currentRectItem != 0){
                    m_graphicsViewVideo->scene()->removeItem(m_currentRectItem);
                    m_currentRectItem = 0;
                }
                m_currentRectItem = m_graphicsViewVideo->scene()->addRect(r, QPen(QBrush(QColor(128, 0, 255)), 1.0 / 1.0));
                m_currentRectItem->setZValue(16);
            }
        }
        else if (ev->type() == QEvent::GraphicsSceneMouseRelease && ev->button() == Qt::LeftButton  ) {
            if (m_currentRectItem != 0){
                if (ev->scenePos().x() > m_graphicsViewVideo->scene()->sceneRect().width() - 20  ||  (ev->scenePos().y()  > m_graphicsViewVideo->scene()->sceneRect().height() - 20)) {
                    m_scenvideo->removeItem(m_currentRectItem);
                }
                else {
                    //std::cout << "Rect !!!!!!!!!!!!!!!" << m_currentRectItem->rect().width() << "  " << m_currentRectItem->rect().width() << std::endl;
                    ++m_selectedRectsCountOnCurrentFrame;
                    QGraphicsRectItem* rectItem = m_graphicsViewVideo->scene()->addRect(
                        m_currentRectItem->rect(), QPen(QBrush(QColor(128, 0, 255)),
                            1.0 / 1.0));
                    m_rectsItem.push_back(rectItem);

                    //std::cout << "Artash codes " << std::endl;
                    cv::Rect tmpRectByUser;
                    tmpRectByUser.x = (int)rectItem->rect().x();
                    tmpRectByUser.y = (int)rectItem->rect().y();
                    tmpRectByUser.width = (int)rectItem->rect().width();
                    tmpRectByUser.height = (int)rectItem->rect().height();
                    m_selectedRectByUser.push_back(tmpRectByUser);
                    m_isCurrentUserSelRectAddedToTracker.push_back(false);
                    //	m_detectedRectsByAlg.push_back(tmpRectByUser); //Artash added for selected rect
                    m_scenvideo->removeItem(m_currentRectItem);
                    m_currentRectItem = 0;
                    ++m_countOfAllRedRasp;

                    std::ostringstream textForCountOfRasbCh;
                    textForCountOfRasbCh << m_countOfAllRedRasp;
                    std::string textForCountOfRasb = "Count Of Straw " + textForCountOfRasbCh.str()/*std::to_string(long(m_countOfAllRedRasp))*/;
                    //if (m_isStartedDetectionAlgorithm) {
                    //	m_countOfRaspLabel->setText(QString(textForCountOfRasb.c_str()));
                    //}
                }
            }
            m_drawRect = false;
        }
        else if (ev->type() == QEvent::GraphicsSceneMouseRelease && ev->button() == Qt::RightButton) {
            if (!m_rectsItem.isEmpty() && m_selectedRectsCountOnCurrentFrame > 0){
                --m_selectedRectsCountOnCurrentFrame;
                m_graphicsViewVideo->scene()->removeItem(m_rectsItem.back());
                m_rectsItem.pop_back();
                m_selectedRectByUser.pop_back();
                m_isCurrentUserSelRectAddedToTracker.pop_back();
                --m_countOfAllRedRasp;

                std::ostringstream textForCountOfRasbCh;
                textForCountOfRasbCh << m_countOfAllRedRasp;
                std::string textForCountOfRasb = "Count Of Straw " +  textForCountOfRasbCh.str()/*std::to_string(long(m_countOfAllRedRasp))*/;

                //if (m_isStartedDetectionAlgorithm) {
                //	m_countOfRaspLabel->setText(QString(textForCountOfRasb.c_str()));
                //}
            }
            m_drawRect = false;
        }
        else if (ev->type() == QEvent::GraphicsSceneMouseDoubleClick && ev->button() == Qt::LeftButton){
            //std::cout << "double click the last " << std::endl;
            unsigned int remElIndex;
            QPointF tmpClickedPoint;
            tmpClickedPoint.setX(ev->scenePos().x());
            tmpClickedPoint.setY(ev->scenePos().y());
            checkAndRemoveRectContainedPoint(tmpClickedPoint,remElIndex);
            //std::cout << "List size of rects Artash " << m_detectedRectsByAlgGraphicsVec.size() << std::endl;

        }
    }
    return QWidget::eventFilter(object, event);
}
