QT += core
QT += gui
QT+= widgets

TARGET = StrawBerryDetectionApp
CONFIG += console
CONFIG -= app_bundle

CONFIG += C++11

INCLUDEPATH += Dependencies/opencv/include

LIBS += -L../Dependencies/opencv/lib  -lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_highgui -lopencv_videoio -lopencv_video -lopencv_tracking
TEMPLATE = app

SOURCES += main.cpp \
    RedStrawDetection.cpp \
    StrawBerryTrack.cpp \
    VideoShowDemo.cpp

HEADERS += \
    RedStrawDetection.h \
    StrawberryTrack.h \
    VideoShowDemo.h

